<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Question;
use App\Http\Controllers\Controller;

class VoteQuestionController extends Controller
{

    public function __invoke(Question $question)
    {
        $vote = (int) request()->vote;

        $voteCount = auth()->user()->voteQuestion($question, $vote);

        if(request()->expectsJson()) {
            return response()->json([
                'message' => "Thanks for the feedback",
                'votesCount' => $voteCount
            ]);
        }

        return back();
    }
}
